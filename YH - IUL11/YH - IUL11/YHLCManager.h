//
//  YHLCManager.h
//  YH - IUL11
//
//  Created by homni_rd01 on 16/6/28.
//  Copyright © 2016年 Homni Electron Inc. All rights reserved.
//
/**
 *  此单例用作CentralManager的代理
 */
#import <Foundation/Foundation.h>
@class YHCentralManager;
@class YHLightingControllerStore;

@protocol YHCentralManagerDelegate <NSObject>
-(void)changecolor:(NSDictionary *)dict withOtherDictionary:(NSDictionary *)dic;
@end
@interface YHLCManager : NSObject


@property  (nonatomic,strong)      YHLightingControllerStore *   store;

+(instancetype)shareManager;

@end
