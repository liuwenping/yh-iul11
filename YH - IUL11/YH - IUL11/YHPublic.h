//
//  YHPublic.h
//  YH - IUL11
//
//  Created by homni_rd01 on 16/6/13.
//  Copyright © 2016年 Homni Electron Inc. All rights reserved.
//

#ifndef YHPublic_h
#define YHPublic_h


#endif /* YHPublic_h */



#pragma mark - HNLight对象若干属性初始值
static int kLightsTotal             = 8;
static int kGroupTotal              = 4;
static NSString *kLights            = @"lights";
static NSString *kDefaultIdentifier = @"00000000-0000-0000-0000-000000000000";
static uint8_t kDefultBrightness    = 255;
static uint8_t kDefultColorR        = 255;
static uint8_t kDefultColorG        = 255;
static uint8_t kDefultColorB        = 255;
static NSString *kDefultLightName   = @"Enter Name";
static int kDefultGroupIndex        = 4;// 表示没有被群组

static NSString *kGroups            = @"groups";
