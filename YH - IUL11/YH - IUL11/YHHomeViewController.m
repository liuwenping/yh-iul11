//
//  YHHomeViewController.m
//  YH - IUL11
//
//  Created by homni_rd01 on 16/6/13.
//  Copyright © 2016年 Homni Electron Inc. All rights reserved.
//

#import "YHHomeViewController.h"
#import "YHEditViewController.h"
#import "YHCoustomLightButton.h"
#import "YHSelectView.h"
#import "YHLight.h"


@interface YHHomeViewController ()<YHSelectViewDelegate ,UIGestureRecognizerDelegate,YHCentralManagerDelegate>
{
    YHCoustomLightButton * buttonView;
    BOOL isSelected;
    UIColor * selectedColor;
    NSUInteger countTag;
}


@property (weak, nonatomic) IBOutlet UIButton *lightSwitchButton;


@property (weak, nonatomic) IBOutlet UISlider *brightnessSlider;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *groupButtons;
@property (weak, nonatomic) IBOutlet UIButton *groupEditButton;
@property (weak, nonatomic) IBOutlet YHSelectView *colorPicker;
@property (nonatomic ,strong)NSMutableArray * colorArray;
@property (nonatomic ,strong)NSMutableArray * selectedColorArray;
@end

@implementation YHHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.colorPicker.delegate = self;
    [self initBrightnessSlider];
    [self GreatView];
    isSelected = NO;
      
    
    _colorArray = [[NSMutableArray alloc]initWithObjects:[UIColor whiteColor],[UIColor whiteColor],[UIColor whiteColor],[UIColor whiteColor],[UIColor whiteColor],[UIColor whiteColor],[UIColor whiteColor],[UIColor whiteColor], nil];
    
    _selectedColorArray = [[NSMutableArray alloc]init];
    

}
-(void)getCurrentColor:(UIColor *)color
{
    const CGFloat  *components = CGColorGetComponents(color.CGColor);
    
    int R = components[0] * 255;
    int G = components[1] * 255;
    int B = components[2] * 255;
    
    if (isSelected == YES) {
        for (int i =0; i < [_scrollView subviews].count; i++) {
            YHCoustomLightButton *selectedView = (YHCoustomLightButton *)[_scrollView viewWithTag:i+1];
            
             if (selectedView.tag == countTag) {
                selectedColor = [UIColor colorWithRed:(R/255.0f) green:(G/255.0f) blue:(B/255.0f) alpha:1.0];
                [_colorArray replaceObjectAtIndex:i withObject:selectedColor];
             
                selectedView.selectColorView.backgroundColor = _colorArray[i];
            }
            else{
                selectedView.selectColorView.backgroundColor = _colorArray[i];
            }
            
           
            
        }
    }
    
}
-(void)GreatView
{
    
    CGFloat Width  = _scrollView.frame.size.width;
    CGFloat Height = _scrollView.frame.size.height;
    [_scrollView setContentSize:CGSizeMake(Width * 2, Height)];
    

    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 2; j ++) {
        buttonView= [[NSBundle mainBundle]loadNibNamed:@"YHCoustomLightButton" owner:nil options:nil][0];
        buttonView.frame = CGRectMake(i * (Width/2), j * Height / 2, Width/2, Height/2);
        buttonView.tag = i+j*4+1;
        buttonView.selectColorView.tag =  i+j*4+1;
        buttonView.groupsImageView.tag = i+j*4+1;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(event:)];
        [buttonView addGestureRecognizer:tap];
            tap.delegate = self;
            //单击View
        [tap setNumberOfTapsRequired:1];
       // 设置View边界的颜色
        buttonView.layer.borderWidth = 1;
        buttonView.layer.borderColor =[UIColor colorWithRed:0.256 green:0.282 blue:0.313 alpha:1].CGColor;
                   [_scrollView addSubview:buttonView];
      }
    }
}

//手势点击方法
-(void)event:(UITapGestureRecognizer *)gesture
{
 
    for (YHCoustomLightButton * selectedView in [_scrollView subviews])
    {
        if (gesture.view == selectedView) {
            
            countTag = selectedView.tag;
            selectedView.layer.borderColor =[UIColor colorWithRed:0 green:0.623 blue:0.909 alpha:1].CGColor;
           
        }
        else{
            
            selectedView.layer.borderColor =[UIColor colorWithRed:0.256 green:0.282 blue:0.313 alpha:1].CGColor;
        }
        
    }

}

- (IBAction)lightSwitchButton:(UIButton *)sender {
    
    isSelected = !isSelected;
    
    
}
//群组编辑的点击事件，切换到编辑页面
- (IBAction)groupEditButton:(UIButton *)sender {
    
    
    YHEditViewController  * editViewController = [[YHEditViewController alloc]initWithNibName:@"YHEditViewController" bundle:[NSBundle mainBundle]];
    
    editViewController.delegate =self;
    //从底部滑入
    [editViewController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    
    [self presentViewController:editViewController animated:YES completion:nil];
    
    
    
}
#pragma mark - YHCentralManagerDelegate  实现代理方法

-(void)changecolor:(NSDictionary *)dict withOtherDictionary:(NSDictionary *)dic
{
    
    //获取字典里的所有values;
    NSArray * dataArray = [dict allValues];
    
    
    if (dataArray) {
        
    for (int i =0; i<dataArray.count; i++) {
        
        NSString * number = dataArray[i];
        int j = [number intValue];
     buttonView = (YHCoustomLightButton *)[self.view viewWithTag:j+1];
        
        NSString * string = [dic objectForKey:dataArray[i]];
        //将数组的值传给groupsImageView
     buttonView.groupsImageView.image = [UIImage imageNamed:string];
   
       }
    
    }
}

- (IBAction)groupButtonAction:(UIButton *)sender {
    
    for (UIButton *button in _groupButtons) {
        if (button.tag == sender.tag) {
            sender.selected=YES;
        }
        else{
            button.selected= NO;
        }
    }

}

- (IBAction)brightnessSliderValueChange:(UISlider *)sender {
    

}
-(void)initBrightnessSlider
{
    //初始化
    if (_brightnessSlider!=nil) {
        [_brightnessSlider setMaximumTrackImage:[UIImage imageNamed:@"cross_small"] forState:UIControlStateNormal];
        [_brightnessSlider setMinimumTrackImage:[UIImage imageNamed:@"cross_selected_small"] forState:UIControlStateNormal];
        [_brightnessSlider setThumbImage:[UIImage imageNamed:@"round_small"] forState:UIControlStateNormal];
        
        _brightnessSlider.maximumValue = 255;
        _brightnessSlider.minimumValue = 0;
        _brightnessSlider.value = 255;
        
    }
    
    
}

@end
