//
//  YHLCManager.m
//  YH - IUL11
//
//  Created by homni_rd01 on 16/6/28.
//  Copyright © 2016年 Homni Electron Inc. All rights reserved.
//

#import "YHLCManager.h"
#import "YHCentralManager.h"
#import "YHLightingControllerStore.h"


@implementation YHLCManager

+(instancetype)shareManager{
    static id shareManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken,^{
        shareManager = [[[self class]alloc]init];
    });
    return shareManager;
}

-(instancetype)init
{
    self = [super init];
    if (self) {
        _store = [[YHLightingControllerStore alloc]init];
        
    }
    return self;
}
@end
