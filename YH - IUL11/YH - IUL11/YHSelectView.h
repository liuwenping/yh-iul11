//
//  YHSelectView.h
//  YH - IUL11
//
//  Created by homni_rd01 on 16/6/20.
//  Copyright © 2016年 Homni Electron Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol YHSelectViewDelegate <NSObject>

-(void)getCurrentColor:(UIColor *)color;

@end

@interface YHSelectView : UIView

@property (nonatomic,weak)id<YHSelectViewDelegate>delegate;



@end
