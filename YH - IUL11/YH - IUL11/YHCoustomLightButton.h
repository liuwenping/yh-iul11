//
//  YHCoustomLightButton.h
//  YH - IUL11
//
//  Created by homni_rd01 on 16/6/15.
//  Copyright © 2016年 Homni Electron Inc. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface YHCoustomLightButton : UIView
@property (weak, nonatomic) IBOutlet UIView *selectColorView;
@property (weak, nonatomic) IBOutlet UIImageView *groupsImageView;
@property (weak, nonatomic) IBOutlet UILabel *groupsName;

@end
