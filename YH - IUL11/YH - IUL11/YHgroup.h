//
//  YHgroup.h
//  YH - IUL11
//
//  Created by homni_rd01 on 16/6/16.
//  Copyright © 2016年 Homni Electron Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YHgroup : NSObject<NSCoding>

@property (copy ,nonatomic) NSArray *lights;
@property (nonatomic) BOOL isEmpty;
@property (nonatomic) BOOL isSelected;
@property (nonatomic) NSInteger groupTag;
@end
