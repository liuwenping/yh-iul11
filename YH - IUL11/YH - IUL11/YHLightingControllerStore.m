//
//  YHLightingControllerStore.m
//  YH - IUL11
//
//  Created by homni_rd01 on 16/6/28.
//  Copyright © 2016年 Homni Electron Inc. All rights reserved.
//

#import "YHLightingControllerStore.h"

@implementation YHLightingControllerStore

+(instancetype)shareStore
{
    static id shareStore = nil;
    static dispatch_once_t onceToken;
    dispatch_once (&onceToken,^{
        shareStore = [[self alloc]init];
    });
    return shareStore;
}

@end
