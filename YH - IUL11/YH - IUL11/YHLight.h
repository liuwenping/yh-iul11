//
//  YHLight.h
//  YH - IUL11
//
//  Created by homni_rd01 on 16/6/16.
//  Copyright © 2016年 Homni Electron Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
@import UIKit;

@interface YHLight : NSObject

@property (strong ,nonatomic) NSString * identifier;
@property (strong ,nonatomic) UIColor  * color;
@property (nonatomic) uint8_t brightness;

//颜色值0-255,使用过程要除以255换算成RGB的float值

@property (nonatomic) uint8_t colorR;

@property (nonatomic) uint8_t colorG;

@property (nonatomic) uint8_t colorB;

@property (copy ,nonatomic) NSString *name;

@property (nonatomic) BOOL isSelected;

/**
 *  记录该灯是否被加入群组
 */
@property (nonatomic) BOOL isGrouped;

/**
 *  记录该灯所在群组的索引(值范围为0-4,4表示未群组)
 */
@property (nonatomic) NSInteger groupIndex;

/**
 *  记录灯的开关状态(关闭时界面显示朦胧状态);
 */
@property (nonatomic) BOOL isOn;

/**
 *  记录该灯是否链接(设备链接成功，会设置为YES)
 */
@property (nonatomic) BOOL isConnect;


@end
