//
//  YHTableViewCell.h
//  YH - IUL11
//
//  Created by homni_rd01 on 16/6/14.
//  Copyright © 2016年 Homni Electron Inc. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface YHTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *lightName;
@property (weak, nonatomic) IBOutlet UIView *lightColorView;
@property (weak, nonatomic) IBOutlet UIImageView *lightGroupImageView;
@property (weak, nonatomic) IBOutlet UIView *contentBackgroundView;


@end
