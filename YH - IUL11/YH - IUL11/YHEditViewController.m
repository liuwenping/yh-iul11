//
//  YHEditViewController.m
//  YH - IUL11
//
//  Created by homni_rd01 on 16/6/13.
//  Copyright © 2016年 Homni Electron Inc. All rights reserved.
//

#import "YHEditViewController.h"
#import "YHTableViewCell.h"
#import "YHLight.h"
#import "YHgroup.h"




@interface YHEditViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
{
    YHTableViewCell *cell;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *groupButtons;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *uploadButton;
@property (weak, nonatomic) IBOutlet UIButton * eraseButton;
@property (nonatomic ,strong) NSMutableDictionary *dicM;
@property (nonatomic ,strong)NSMutableDictionary *dicN;
@property (nonatomic ,strong) NSMutableArray * dataSoureArray;
@property (nonatomic ,strong) NSIndexPath *selectedIndexPath;


@end

@implementation YHEditViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _dicM = [NSMutableDictionary dictionary];
    _dicN = [NSMutableDictionary dictionary];
  
    //给tableView注册一个cell
    [_tableView registerNib:[UINib nibWithNibName:@"YHTableViewCell" bundle:nil] forCellReuseIdentifier:@"mycell"];
    [self groupEdit:_groupButtons[0]];
    
}
- (IBAction)upload:(UIButton *)sender {
}
- (IBAction)erase:(UIButton *)sender {
}
- (IBAction)groupEdit:(UIButton *)sender {
    
    for (UIButton *button in _groupButtons) {
        if (button.tag == sender.tag) {
            sender.selected=YES;
        }
        else{
            button.selected= NO;
        }
    }
    
    
    //刷新
   [_tableView reloadData];
    
}
- (IBAction)save:(UIButton *)sender {
    
    //执行回调(反向传值)
    //如果代理响应这个事件
    if ([_delegate respondsToSelector:@selector(changecolor:withOtherDictionary:)])
    {
        //那么才来执行
        //这里就是调用协议中的方法
        [_delegate changecolor:_dicM withOtherDictionary:_dicN];
    }
    
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)cannel:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark-UITableViewDataSource&UITableViewDelegate

// MARK: - UITableViewDataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 8;
 }
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
        cell =[tableView dequeueReusableCellWithIdentifier:@"mycell"];
    
        cell.lightName.text = @"Enter Name";
        return cell;
}

//tableView被选中
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    cell = [_tableView cellForRowAtIndexPath:indexPath];
    
    NSString *row = [NSString stringWithFormat:@"%ld",(long)indexPath.row];
    
    for (int i = 0; i<_groupButtons.count; i++) {
        
        UIButton  * groupButton = _groupButtons[i];
        if (groupButton.selected ==YES) {
            switch (groupButton.tag) {
                case 0:
                    cell.lightGroupImageView.image = [UIImage imageNamed:@"box_group1"];
                    [_dicN setObject:@"box_group1" forKey:row];
                    break;
                case 1:
                    cell.lightGroupImageView.image = [UIImage imageNamed:@"box_group2"];
                    [_dicN setObject:@"box_group2" forKey:row];
                    break;
                case 2:
                    cell.lightGroupImageView.image = [UIImage imageNamed:@"box_group3"];
                    [_dicN setObject:@"box_group3" forKey:row];
                    break;
                case 3:
                    cell.lightGroupImageView.image = [UIImage imageNamed:@"box_group4"];
                    [_dicN setObject:@"box_group4" forKey:row];
                default:
                    break;
            }
        }
    }
    
    [_dicM setObject:row forKey:_dicN];
    
}

//取消选中
-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.lightGroupImageView.image = [UIImage imageNamed:@"box_empty"];
    
}


// MARK: - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return tableView.frame.size.height / 8;
    
    
}

@end
