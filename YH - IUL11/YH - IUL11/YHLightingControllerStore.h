//
//  YHLightingControllerStore.h
//  YH - IUL11
//
//  Created by homni_rd01 on 16/6/28.
//  Copyright © 2016年 Homni Electron Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YHLightingControllerStore : NSObject

@property (strong, nonatomic) NSArray *allLights;

//  返回已经链接light的数量
@property (nonatomic) int connectedLightTotal;

@property (readonly, nonatomic) NSArray *allGroups;

+ (instancetype)shareStore;


@end
