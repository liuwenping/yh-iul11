//
//  AppDelegate.m
//  YH - IUL11
//
//  Created by homni_rd01 on 16/6/13.
//  Copyright © 2016年 Homni Electron Inc. All rights reserved.
//

#import "AppDelegate.h"
#import "YHHomeViewController.h"

@interface AppDelegate ()<UIAlertViewDelegate>

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"请选择" message:@"WIFI OR BaByBluetooth or IUL11Test" delegate:self cancelButtonTitle:@"IUL11" otherButtonTitles:@"BaByBluetooth",@"WIFI", nil];
    
    [alert show];
    
   
   
return YES;
}
#pragma mark - UIAlertViewDelegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (buttonIndex==2||buttonIndex==1) {
        YHHomeViewController *homeVC = [[YHHomeViewController alloc]init];
        [self .window.rootViewController presentViewController:homeVC animated:YES completion:nil];
    }else if (buttonIndex ==0)
    {
        return;
    }
}
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
