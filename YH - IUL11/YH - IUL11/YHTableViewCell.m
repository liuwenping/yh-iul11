//
//  YHTableViewCell.m
//  YH - IUL11
//
//  Created by homni_rd01 on 16/6/14.
//  Copyright © 2016年 Homni Electron Inc. All rights reserved.
//

#import "YHTableViewCell.h"

@implementation YHTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
   }

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    if (selected) {
        _lightColorView.backgroundColor = [UIColor whiteColor];
        _contentBackgroundView.backgroundColor = [UIColor colorWithRed:0.650 green:0.760 blue:0.262 alpha:0.5];
        
    }
    else {
        _lightColorView.backgroundColor = [UIColor whiteColor];
    }
}
@end
