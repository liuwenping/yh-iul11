//
//  YHCentralManager.h
//  YH - IUL11
//
//  Created by homni_rd01 on 16/6/28.
//  Copyright © 2016年 Homni Electron Inc. All rights reserved.
//
/**
 *  Central:用于扫描并监听蓝牙模块设备
 */

#import <Foundation/Foundation.h>

@interface YHCentralManager : NSObject

@end
