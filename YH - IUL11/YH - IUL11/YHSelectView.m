//
//  YHSelectView.m
//  YH - IUL11
//
//  Created by homni_rd01 on 16/6/20.
//  Copyright © 2016年 Homni Electron Inc. All rights reserved.
//

#define MYWIDTH self.bounds.size.width
#define MYHEIGHT self.bounds.size.height
#define MYCENTER CGPointMake(self.bounds.size.width *0.5 , self.bounds.size.height*0.5)




#import "YHSelectView.h"
#import "YHHomeViewController.h"
@interface YHSelectView()

@property (nonatomic , strong)UIImageView * centerImage;  // 中间的图片

@end

@implementation YHSelectView

-(instancetype)initWithFrame:(CGRect)frame
{
    if (self = [ super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if ( self  = [super initWithCoder:aDecoder]) {
        self.backgroundColor = [UIColor  colorWithRed:0.133 green:0.145 blue:0.172 alpha:1];
        
    }
    return self;
    
}

-(void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    UIImage * imageCenter = [UIImage imageNamed:@"color_range"];
        [imageCenter drawInRect:CGRectMake((MYWIDTH - MYHEIGHT )/2, 0, MYHEIGHT, MYHEIGHT )];
    //设置小圆圈的中心位置
    self.centerImage = [[UIImageView alloc]initWithFrame:CGRectMake(MYCENTER.x - 15, MYCENTER.y - 15, 30, 30)];
    self.centerImage.image = [UIImage imageNamed:@"circle"];
    [self addSubview:self.centerImage];

 }

//触摸开始
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event

{
    UITouch * touch = [touches anyObject ];
    
    CGPoint  currentPoint = [touch locationInView:self];
    
    CGFloat chassRadius = (MYHEIGHT )*0.5 - MYWIDTH/20;
    NSLog(@"chassRadius:%f",chassRadius);
    CGFloat absDistanceX = fabs(currentPoint.x - MYCENTER.x);
    CGFloat absDistanceY = fabs(currentPoint.y - MYCENTER.y);
    CGFloat currentTopointRadius = sqrtf(absDistanceX  * absDistanceX + absDistanceY *absDistanceY);
    NSLog(@"currentRadius:%f",currentTopointRadius);
    if(currentTopointRadius < chassRadius){//实在色盘上面
        
        self.centerImage.center =  currentPoint;
        UIColor *color = [self getPixelColorAtLocation:currentPoint];
        if(self.delegate && [self.delegate respondsToSelector:@selector(getCurrentColor:)]){
            
            [self.delegate getCurrentColor:color];
        }

    }

}

-(void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event

{
    UITouch *touch = [touches anyObject];
    CGPoint currentPoint = [touch locationInView:self];
    
    CGFloat chassisRadius =(MYHEIGHT )*0.5 - MYWIDTH/20;
    CGFloat absDistanceX = (currentPoint.x - MYCENTER.x);
    CGFloat absDistanceY = (currentPoint.y - MYCENTER.y);
    CGFloat currentTopointRadius = sqrtf(absDistanceX * absDistanceX + absDistanceY *absDistanceY);
    
    if (currentTopointRadius <chassisRadius) {
        //取色
        self.centerImage.center = currentPoint;
        UIColor *color = [self getPixelColorAtLocation:currentPoint];
        if(self.delegate && [self.delegate respondsToSelector:@selector(getCurrentColor:)]){
            
            [self.delegate getCurrentColor:color];
        }
    }

    
}
- (UIColor*) getPixelColorAtLocation:(CGPoint)point {
    
        UIColor* color = nil;
        UIGraphicsBeginImageContext(self.bounds.size);
        [self.layer renderInContext:UIGraphicsGetCurrentContext()];
        UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
        CGImageRef inImage = viewImage.CGImage;
        
        
        CGContextRef cgctx = [self createARGBBitmapContextFromImage:inImage];
        if (cgctx == NULL) { return nil;}
        
        size_t w = self.bounds.size.width;
        size_t h = self.bounds.size.height;
        
        CGRect rect = {{0,0},{w,h}};
        
        CGContextDrawImage(cgctx, rect, inImage);
        
        
        unsigned char* data = CGBitmapContextGetData (cgctx);
        if (data != NULL) {
                        int offset = 4*((w*round(point.y))+round(point.x));
            int alpha =  data[offset];
            int red   =  data[offset+1];
            int green =  data[offset+2];
            int blue  =  data[offset+3];
            
            color = [UIColor colorWithRed:(red/255.0f) green:(green/255.0f) blue:(blue/255.0f) alpha:(alpha/255.0f)];
        }
        
      
        CGContextRelease(cgctx);
        
        if (data) { free(data); }
        
        return color;
    }
   
- (CGContextRef) createARGBBitmapContextFromImage:(CGImageRef) inImage {
    
    CGContextRef     context = NULL;
    CGColorSpaceRef  colorSpace;
    void *           bitmapData;
    long             bitmapByteCount;
    long             bitmapBytesPerRow;
    
    size_t pixelsWide = self.bounds.size.width;
    size_t pixelsHigh = self.bounds.size.height;
    
    
    bitmapBytesPerRow   = (pixelsWide * 4);
    bitmapByteCount     = (bitmapBytesPerRow * pixelsHigh);
    
    
    colorSpace = CGColorSpaceCreateDeviceRGB();
    
    if (colorSpace == NULL)
    {
        fprintf(stderr, "Error allocating color space\n");
        return NULL;
    }
        bitmapData = malloc( bitmapByteCount );
    if (bitmapData == NULL)
    {
        fprintf (stderr, "Memory not allocated!");
        CGColorSpaceRelease( colorSpace );
        return NULL;
    }
    
        context = CGBitmapContextCreate (bitmapData,
                                     pixelsWide,
                                     pixelsHigh,
                                     8,                                           bitmapBytesPerRow,
                                     colorSpace,
                                     kCGImageAlphaPremultipliedFirst);
    if (context == NULL)
    {
        free (bitmapData);
        fprintf (stderr, "Context not created!");
    }
    
        CGColorSpaceRelease( colorSpace );
    
    return context;
}


@end
