//
//  YHLight.m
//  YH - IUL11
//
//  Created by homni_rd01 on 16/6/16.
//  Copyright © 2016年 Homni Electron Inc. All rights reserved.
//

#import "YHLight.h"
#import "YHPublic.h"

@implementation YHLight
-(instancetype)init
{
    self = [super init];
    if (self) {
        _identifier =kDefaultIdentifier;
        _color      = [UIColor whiteColor];
        _brightness = kDefultBrightness;
        _colorR     = kDefultColorR;
        _colorG     = kDefultColorG;
        _colorB     = kDefultColorB;
        _name       = kDefultLightName;
        _isGrouped  = NO;
        _groupIndex = kDefultGroupIndex;
        _isSelected = NO;
        _isOn       = NO;
        _isConnect  = NO;
    }
    
    return self;
}
// 解固数据
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self) {
        _identifier = [aDecoder decodeObjectForKey:@"identifier"];
        _color      = [aDecoder decodeObjectForKey:@"color"];
        _brightness = [aDecoder decodeIntForKey:@"brightness"];
        _colorR     = [aDecoder decodeIntForKey:@"colorR"];
        _colorG     = [aDecoder decodeIntForKey:@"colorG"];
        _colorB     = [aDecoder decodeIntForKey:@"colorB"];
        _name       = [aDecoder decodeObjectForKey:@"name"];
        _isGrouped  = [aDecoder decodeBoolForKey:@"isGrouped"];
        _groupIndex = [aDecoder decodeIntegerForKey:@"groupIndex"];
        _isSelected = [aDecoder decodeBoolForKey:@"isSelected"];
        _isOn       = [aDecoder decodeBoolForKey:@"isOn"];
        _isConnect  = [aDecoder decodeBoolForKey:@"isConnect"];
    }
    return self;
}

// 固化数据
- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.identifier forKey:@"identifier"];
    [aCoder encodeObject:self.color forKey:@"color"];
    [aCoder encodeInt:self.brightness forKey:@"brightness"];
    [aCoder encodeInt:self.colorR forKey:@"colorR"];
    [aCoder encodeInt:self.colorG forKey:@"colorG"];
    [aCoder encodeInt:self.colorB forKey:@"colorB"];
    [aCoder encodeObject:self.name forKey:@"name"];
    [aCoder encodeBool:self.isGrouped forKey:@"isGrouped"];
    [aCoder encodeInteger:self.groupIndex forKey:@"groupIndex"];
    [aCoder encodeBool:self.isSelected forKey:@"isSelected"];
    [aCoder encodeBool:self.isOn forKey:@"isOn"];
    [aCoder encodeBool:self.isConnect forKey:@"isConnect"];
}


@end
