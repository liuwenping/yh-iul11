//
//  YHgroup.m
//  YH - IUL11
//
//  Created by homni_rd01 on 16/6/16.
//  Copyright © 2016年 Homni Electron Inc. All rights reserved.
//

#import "YHgroup.h"

@implementation YHgroup
-(instancetype)init

{
    self = [super init];
    if (self) {
        _lights = [[NSArray alloc]init];
        _isEmpty    = YES;
        _isSelected = NO;
    }
    return self;
}
-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self) {
        _lights = [aDecoder decodeObjectForKey:@"lights"];
        _isEmpty = [aDecoder decodeBoolForKey:@"isEmpty"];
        _isSelected = [aDecoder decodeBoolForKey:@"isSelected"];
    }
    return self;
    
}
-(void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeObject:self.lights forKey:@"light"];
    [aCoder encodeBool:self.isEmpty forKey:@"isEmpty"];
    [aCoder encodeBool:self.isSelected forKey:@"isSelected"];
}
@end
